﻿using System;
using System.IO;

namespace cmaker
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string lists = "cmake_minimum_required( VERSION 3.0 )\r\n"
                + "\r\nproject( PAD2 )\r\n"
                + "\r\nset(CMAKE_BINARY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Build)\r\n"
                + "set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/Build )	\r\n"
                + "set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/Build )	\r\n"
                + "set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/Build )	\r\n";

            foreach (string dir in Directory.EnumerateDirectories(path))
            {
                var s = dir.Split('\\');
                var newDir = s[s.Length - 1];
                if (newDir == ".git") continue;
                Console.WriteLine("\n\nDirectory: " + newDir);
                string sub_list = "cmake_minimum_required( VERSION 3.0 )\r\n\r\n"
                    + "add_executable(" + newDir + " ";
                bool dirOK = false;
                bool cs = false;
                foreach (string file in Directory.EnumerateFiles(dir))
                {

                    s = file.Split('\\');
                    var newFile = s[s.Length - 1];
                    Console.WriteLine("File: " + newFile);
                    if (newFile.EndsWith(".cpp") || newFile.EndsWith(".h") || newFile.EndsWith(".hpp") || newFile.EndsWith(".c"))
                    {
                        sub_list += newFile + " ";
                        dirOK = true;
                    }
                }
                if (!dirOK) continue;
                sub_list += ")\r\n";
                File.WriteAllText(dir + "\\CMakeLists.txt", sub_list);
                lists += "add_subdirectory(" + newDir + ")\r\n";
            }
            //lists += "set_property(DIRECTORY ${ CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT pad2_praktikum1)";

            File.WriteAllText(path + "\\CMakeLists.txt", lists);
        }
    }
}
