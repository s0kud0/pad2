#include "HotelBooking.h"


HotelBooking::HotelBooking(long id, double price, long travelId, string fromDate, string toDate, string hotel, string town)
    : Booking(id, price, travelId, fromDate, toDate), hotel(hotel), town(town)
{
}

HotelBooking::~HotelBooking()
{
}
