#pragma once
#include <string>

using namespace std;

class Booking
{
protected:
	long id;
	double price;
	long travelId;
	string fromDate;
	string toDate;
public:
    Booking(long id, double price, long travelId, string fromDate, string toDate);
	virtual ~Booking();

	long GetId();
};

