#include "TravelAgency.h"
#include <vector>

int main()
{
	TravelAgency travelAgency;
    travelAgency.ReadFile();
    string bookingData[3] = { "Muenchen","Berlin","AirBerlin" };
    vector<string> data;
    data.assign(bookingData, bookingData + 3);
    travelAgency.createBooking('F', 12, "Heute", "Gestern", 1, data);
    system("pause");
    return 0;
}
