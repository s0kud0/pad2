#include "TravelAgency.h"

TravelAgency::TravelAgency()
{
}


TravelAgency::~TravelAgency()
{	
	for (size_t i = 0; i < allBookings.size(); i++)
	{
        if (allBookings[i])
        {
            delete allBookings[i];
            allBookings[i] = 0;
        }
	}
}


void TravelAgency::ReadFile()
{
	fstream fileStream;
    fileStream.open("bookings2.txt", fstream::in);
	if (!fileStream.is_open())
	{
		cout << "Unable to open file" << endl;
		return;
	}

	vector<string> bookings;

	while (!fileStream.eof())
	{
		string currentLine;
		getline(fileStream, currentLine);
		bookings.push_back(currentLine);
	}
    fileStream.close();

	for (size_t i = 0; i < bookings.size(); i++)
	{
		vector<string> tokens = split(bookings[i], '|');

		Customer* currentCustomer = nullptr;
		Travel* currentTravel = nullptr;

		if ((currentCustomer = findCustomer(stol(tokens[6]))) == nullptr)
		{
			currentCustomer = new Customer(stol(tokens[6]), tokens[7]);
			allCustomers.push_back(currentCustomer);
			++numCustomers;
		}

		if ((currentTravel = findTravel(stol(tokens[5]))) == nullptr)
		{
			currentTravel = new Travel(stol(tokens[5]), stol(tokens[6]));
			allTravels.push_back(currentTravel);
			currentCustomer->addTravel(currentTravel);
			++numTravels;
		}

		if (tokens[0] == "F")
		{
			FlightBooking* flight = new FlightBooking(stol(tokens[1]), stod(tokens[2]), stol(tokens[5]), tokens[3], tokens[4], tokens[8], tokens[9], tokens[10]);
			allBookings.push_back(flight);
			currentTravel->addBooking(flight);
			price += stod(tokens[2]);
			++numflights;
		}
		else if (tokens[0] == "H")
		{
			HotelBooking* hotel = new HotelBooking(stol(tokens[1]), stod(tokens[2]), stol(tokens[5]), tokens[3], tokens[4], tokens[8], tokens[9]);
			allBookings.push_back(hotel);
			currentTravel->addBooking(hotel);
			price += stod(tokens[2]);
			++numhotels;
		}
		else
		{
			RentalCarReservation* car = new RentalCarReservation(stol(tokens[1]), stod(tokens[2]), stol(tokens[5]), tokens[3], tokens[4], tokens[8], tokens[9], tokens[10]);
			allBookings.push_back(car);
			currentTravel->addBooking(car);
			price += stod(tokens[2]);
			++numcars;
		}
	}
    this->Print();
}

Booking * TravelAgency::findBooking(long id)
{
	for (size_t i = 0; i < allBookings.size(); i++)
	{
		if (allBookings[i]->GetId() == id)
		{
			return allBookings[i];
		}
	}
	return nullptr;
}

Travel * TravelAgency::findTravel(long id)
{
	for (size_t i = 0; i < allTravels.size(); i++)
	{
		if (allTravels[i]->GetId() == id)
		{
			return allTravels[i];
		}
	}
	return nullptr;
}

Customer * TravelAgency::findCustomer(long id)
{
	for (size_t i = 0; i < allCustomers.size(); i++)
	{
		if (allCustomers[i]->GetId() == id)
		{
			return allCustomers[i];
		}
	}
	return nullptr;
}

int TravelAgency::createBooking(char type, double price, string start, string end, long travelId, vector<string> bookingDetails)
{
	Travel* currentTravel = nullptr;
	if ((currentTravel = findTravel(travelId)) == nullptr)
	{
		cout << "Reise konnte nicht gefunden werden!" << endl;
		return -1;
	}

	int lastId = 0;
	for (size_t i = 0; i < allBookings.size(); i++)
	{
		if (allBookings[i]->GetId() > lastId)
		{
			lastId = allBookings[i]->GetId();
		}
	}
	lastId++;
    this->price += price;
	if (type == 'F')
	{
		FlightBooking* flight = new FlightBooking(lastId, price, travelId, start, end, bookingDetails[0], bookingDetails[1], bookingDetails[2]);
		allBookings.push_back(flight);
		currentTravel->addBooking(flight);
        ++this->numflights;
	}
	else if (type == 'H')
	{
		HotelBooking* hotel = new HotelBooking(lastId, price, travelId, start, end, bookingDetails[0], bookingDetails[1]);
		allBookings.push_back(hotel);
		currentTravel->addBooking(hotel);
        ++this->numhotels;
	}
	else
	{
		RentalCarReservation* car = new RentalCarReservation(lastId, price, travelId, start, end, bookingDetails[0], bookingDetails[1], bookingDetails[2]);
		allBookings.push_back(car);
        currentTravel->addBooking(car);
        ++this->numcars;
	}

	cout << "Eine Buchung vom typ " << type << " wurde angelegt. Preis: " << price << " Euro" << endl;
    this->Print();
    return lastId;
}

void TravelAgency::Print()
{
    cout << "Es wurden " << numflights << " Flugreservierungen, " << numhotels << " Hotelbuchungen und " << numcars << " Mietwagenreservierungen im Wert von " << price << " Euro eingelesen." << endl;
    cout << "Es wurden " << numTravels << " Reisen und " << numCustomers << " Kunden angelegt." << endl;
    cout << "Der Kunde mit der ID 1 hat " << this->findCustomer(1)->getTravelCount() << " Reisen Gebucht." << endl;
    cout << "Zur Reise mit der ID 17 gehören " << this->findTravel(17)->getBookingCount() << " Buchungen" << endl;
}

//Splits a string like string.split function in C#
// Parameter &s = String to split
// Parameter &delimiter = char to split at
vector<string> TravelAgency::split(const string &s, char delimiter)
{
	vector<string> elements;
	stringstream ss(s);
	string item;
	while (getline(ss, item, delimiter))
	{
		elements.push_back(item);
	}

	return elements;
}
