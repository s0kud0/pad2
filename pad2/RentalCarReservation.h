#pragma once
#include "Booking.h"
#include <string>

using namespace std;

class RentalCarReservation : public Booking
{
private:
    string pickupLocation;
	string returnLocation;
	string company;
public:
	RentalCarReservation(long id, double price, long travelId, string fromDate, string toDate, string pickupLocation, string returnLocation, string company);
	virtual ~RentalCarReservation();
};

