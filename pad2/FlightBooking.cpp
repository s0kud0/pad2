#include "FlightBooking.h"


FlightBooking::FlightBooking(long id, double price, long travelId, string fromDate, string toDate, string fromDest, string toDest, string airline)
    : Booking(id, price, travelId, fromDate, toDate), fromDest(fromDest), toDest(toDest), airline(airline)
{
}

FlightBooking::~FlightBooking()
{
}
