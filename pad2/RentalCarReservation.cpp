#include "RentalCarReservation.h"


RentalCarReservation::RentalCarReservation(long id, double price, long travelId, string fromDate, string toDate, string pickupLocation, string returnLocation, string company)
    : Booking(id, price, travelId, fromDate, toDate), pickupLocation(pickupLocation), returnLocation(returnLocation), company(company)
{
}

RentalCarReservation::~RentalCarReservation()
{
}
