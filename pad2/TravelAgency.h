#pragma once
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include "Booking.h"
#include "FlightBooking.h"
#include "HotelBooking.h"
#include "RentalCarReservation.h"
#include "Travel.h"
#include "Customer.h"

using namespace std;

class TravelAgency
{
private:
	vector<Booking*> allBookings;
	vector<Customer*> allCustomers;
	vector<Travel*> allTravels;
    double price = 0;
    int numflights = 0;
    int numhotels = 0;
    int numcars = 0;
    int numCustomers = 0;
    int numTravels = 0;

	vector<string> split(const string &s, char delimiter);

public:
	TravelAgency();
	virtual ~TravelAgency();
	
	void ReadFile();
	Booking* findBooking(long id);
	Travel* findTravel(long id);
	Customer* findCustomer(long id);
	int createBooking(char type, double price, string start, string end, long travelId, vector<string> bookingDetails);

    void Print();
};

