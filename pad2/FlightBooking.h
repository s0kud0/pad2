#pragma once
#include "Booking.h"
#include <string>

using namespace std;

class FlightBooking : public Booking
{
private:
	//long id;
	//double price;
	//string fromDate;
	//string toDate;
	string fromDest;
	string toDest;
	string airline;
public:
	FlightBooking(long id, double price, long travelId, string fromDate, string toDate, string fromDest, string toDest, string airline);
	virtual ~FlightBooking();
};

