#pragma once
#include <vector>
#include "Booking.h"

class Travel
{
private: 
	long id;
	long customerId;
	vector<Booking*> travelBooking;
public:
	Travel(long id, long customerId);
	virtual ~Travel();

	long GetId();
	void addBooking(Booking* booking);
    int getBookingCount();
};

